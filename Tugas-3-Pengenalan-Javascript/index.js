
// soal 1

var kalimatPertama = "saya sangat senang hari ini";
var kalimatKedua = "belajar javascript itu keren";

var kalimatHasil = kalimatPertama.substring(0, 4) + kalimatPertama.substring(11, 19) + kalimatKedua.substring(0, 7) + kalimatKedua.substring(7, 19).toUpperCase();

console.log(kalimatHasil)

// soal 2

var angka1 = parseInt("10");
var angka2 = parseInt("2");
var angka3 = parseInt("4");
var angka4 = parseInt("6");

var hasil = angka1+angka2*angka3+angka4;
console.log(hasil)

// soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14);  
var kataKetiga = kalimat.substring(15, 18);  
var kataKeempat = kalimat.substring(19, 24); 
var kataKelima = kalimat.substring(25, 31); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);