
// soal 1
var nilai = parseInt("85");

if ( nilai >= 85 ){
    console.log("A");
} else if ( nilai >= 75 && nilai < 85 ){
    console.log("B");
} else if ( nilai >= 65 && nilai < 75 ){
    console.log("C");
} else if ( nilai >= 55 && nilai < 65 ){
    console.log("D");
} else if ( nilai < 55 ){
    console.log("E");
}

// soal 2
console.log('');

var tanggal = 18;
var bulan = 11;
var tahun = 2004;

switch (bulan) {
    case 1 : { console.log(tanggal + " Januari " + tahun); break; }
    case 2 : { console.log(tanggal + " Februari " + tahun); break; }
    case 3 : { console.log(tanggal + " Maret " + tahun); break; }
    case 4 : { console.log(tanggal + " April " + tahun); break; }
    case 5 : { console.log(tanggal + " Mei " + tahun); break; }
    case 6 : { console.log(tanggal + " Juni " + tahun); break; }
    case 7 : { console.log(tanggal + " Juli " + tahun); break; }
    case 8 : { console.log(tanggal + " Agustus " + tahun); break; }
    case 9 : { console.log(tanggal + " September " + tahun); break; }
    case 10 : { console.log(tanggal + " Oktober " + tahun); break; }
    case 11 : { console.log(tanggal + " November " + tahun); break; }
    case 12 : { console.log(tanggal + " Desember " + tahun); break; }
    default : { console.log("bulan tidak diketahui"); }
}

// soal 3
console.log('');

var n = 7;
var hasil = '';
for (var i = 0; i < n; i++) {
    for (var j = 0; j <= i; j++) {
       hasil += '#';
    }
    hasil += '\n';
}
console.log(hasil);

// soal 4
var m = 10;
var kata = [' ', 'programming', 'Javascript', 'VueJS'];
var hasil = '';
for (var i = 1; i <= m; i++) {
    if (i % 3 != 0) {
        hasil += i + ' - I love ' + kata[i%3] + "\n";
    } else if(i % 3 == 0) {
        hasil += i + ' - I love ' + kata[3] + "\n";
        for (var j = 1; j <=i; j++){
            hasil += '=';
        }
        hasil += '\n';
    }
    
}

console.log(hasil);
