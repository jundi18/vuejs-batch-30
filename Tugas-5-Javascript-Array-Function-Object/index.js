// soal 1
console.log("----soal 1----")
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"]
daftarHewan.sort()

for (var i = 0; i < daftarHewan.length; i++) { 
    console.log(daftarHewan[i])
}

// soal 2
console.log("----soal 2----")
function introduce (data) {
    return "Nama saya " + data.name + ", umur saya " + data.age + ", alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby;    
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
// Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"  
var perkenalan = introduce(data)
console.log(perkenalan) 

// soal 3
console.log("----soal 3----")

function hitung_huruf_vokal(data){
    var jum = 0
    var huruf_vokal = ["a", "i", "u", "e", "o"]
    var str_low_case = data.toLowerCase()
    var arr_str = []
    
    for (var i = 0; i < str_low_case.length; i++) {
        arr_str.push(str_low_case.charAt(i))
    }
    
    for(var j = 0; j < arr_str.length; j++) {
        for (var k = 0; k < huruf_vokal.length; k++) {
            if (huruf_vokal[k] == arr_str[j]) {
                jum++
            }
        } 
    }
    return jum
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Ikmal")

console.log(hitung_1, hitung_2)

// soal 4

console.log("----soal 4----")

function hitung (angka) {
    if(angka == 0) {
        return -2
    } else if(angka == 1) {
        return 0
    } else {
        return hitung(angka-1) + 2
    }
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8)


