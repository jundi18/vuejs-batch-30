
// Soal 1
console.log("===Soal 1===")

const luasPP = (p, l) =>  console.log(p*l) 
const kelilingPP = (p, l) => console.log(2*(p+l)) 
 
luasPP(7, 5)
kelilingPP(6, 5)
 
// Soal 2
console.log("===Soal 2===")

const newFunction  = (firstName, lastName) => console.log(`${firstName} ${lastName}`)
//Driver Code 
newFunction("William", "Imoh")

// Soal 3

console.log('===Soal 3===')

// Diberikan sebuah objek sebagai berikut:
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject

// Driver code
console.log(firstName, lastName, address, hobby)

// Soal 4

console.log('===Soal 4===')

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]
//Driver Code
console.log(combined)

// Soal 5

console.log('===Soal 5===')

const planet = "earth" 
const view = "glass" 

var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
var after = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}` 

console.log(before)
console.log(after)